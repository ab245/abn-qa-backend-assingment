# Backend Test Automation Assignment

## Requirements
### Environment
* GitLab account
* Java 11 (JDK) (optionally)
* Maven 3.6+ (optionally)
* Any IDE you comfortable with (eg. IntelliJ, VSCode)

### Skills
* Java 8+ (coding standards)
* Clean Code
* Maven
* Git & GitLab

### Instructions
Fork this project from here (with Public visibility), example: 
![img.png](doc/img/01_fork_project.png)

#### Working in Web IDE (preferable)

1. Open Project in GitPod:
   ```
   https://gitpod.io/#https://gitlab.com/{group-name}/{forked-project-name}
   ```
   Example: 
   ```
   https://gitpod.io/#https://gitlab.com/ab245/abn-qa-backend-assingment
   ```
2. Sing-in with GitLab account
3. Create and commit your solution into your forked repository
4. Create documentation in the README.md under the `Documentation` section
5. Enable Repository permissions for GitPod when coding from Web IDE here: https://gitpod.io/integrations
   ![img.png](doc/img/02_enable_repo_permissions.png)

## Documentation
_<< Your documentation comes here >>_